#!/bin/bash


cat <<EOF >./in
#!/bin/bash
echo "Hello World!"
EOF
chmod +x ./in
./in >./expected

gzexe ./in 2>/dev/null
/bin/bash ./in >./out
diff ./out ./expected
result=$?
rm -f ./in ./in~ ./out ./expected
exit $result

