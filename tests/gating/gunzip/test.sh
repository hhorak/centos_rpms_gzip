#!/bin/bash

echo "Hello World!" > ./in
gzip -f -k ./in
gunzip -c ./in.gz > ./out
diff ./in ./out
result=$?
rm -f *.gz ./out ./in
exit $result

