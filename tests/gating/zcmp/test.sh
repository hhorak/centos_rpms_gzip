#!/bin/bash

echo "Hello World!" > ./in
gzip -f -k ./in
zcmp ./in ./in.gz
result=$?
rm -f *.gz ./out ./in
exit $result

