#!/bin/bash

echo "Hello World!" > ./in
gzip -f -k ./in
zfgrep "ello" ./in.gz >/dev/null
result=$?
rm -f *.gz ./out ./in
exit $result

